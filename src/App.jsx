import { useState } from "react"
import PdfViewerComponent from "./Pspdfkit"
// import pdf from './TEST.pdf'

const App = () => {
  const [pdf, setPdf] = useState(null)


// function localPdfFileToByteArray(filePath) {


//   try {
//     // Create a new XMLHttpRequest object
//     const xhr = new XMLHttpRequest();
    
//     // Set the responseType to arraybuffer to get the file content as a byte array
//     // xhr.responseType = 'arraybuffer';
    
//     // Open the file at the specified path
//     xhr.open('GET', filePath, false);
    
//     // Send the request
//     xhr.send();
    
//     // Check if the request was successful
//     if (xhr.status === 200) {
//       // Return the file content as a byte array
//       setPdf(new Uint8Array(xhr.response));
//       console.log(new Uint8Array(xhr.response))
//     } else {
//       // Log the error
//       console.error(`Error: ${xhr.status} ${xhr.statusText}`);
//       return null;
//     }
//   } catch (error) {
//     // Log the error
//     console.error(`Error: ${error}`);
//     return null;
//   }
// }

function readFile(file) {
  return new Promise((resolve, reject) => {
    // Create file reader
    let reader = new FileReader()

    // Register event listeners
    reader.addEventListener("loadend", e => resolve(e.target.result))
    reader.addEventListener("error", reject)

    // Read file
    reader.readAsArrayBuffer(file)
  })
}


async function getAsByteArray(file) {
  // let returns =  new Uint8Array(await readFile(file))

  let pdfBlob = new Blob([file], { type: 'text/pdf' });

  let pdfUrl = URL.createObjectURL(pdfBlob);

  console.log(pdfUrl)
  setPdf(pdfUrl)
}


  return (
   <>
   <input type="file" onChange={(v) => getAsByteArray(v.target.files[0])} />
    <div style={{ display: "block", width: "100%", height: "100vh" }}>
    {
      pdf &&  <PdfViewerComponent
      document={pdf}
    />
    }
    </div>
   </>
  )
}

export default App
