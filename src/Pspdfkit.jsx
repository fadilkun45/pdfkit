import { useEffect, useRef } from "react";

export default function PdfViewerComponent({document}) {
  const containerRef = useRef(null);

  useEffect(() => {
    const container = containerRef.current; // This `useRef` instance will render the PDF.

    let PSPDFKit, instance;
    
    (async function () {
      PSPDFKit = await import("pspdfkit")

      const downloadButton = {
        type: "custom",
        id: "download-pdf",
        icon: "/download.svg",
        title: "Download",
        onPress: () => {
          instance.exportPDF().then((buffer) => {
            const blob = new Blob([buffer], { type: "application/pdf" });
            const fileName = "document.pdf";
            if (window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(blob, fileName);
            } else {
              const objectUrl = URL.createObjectURL(blob);
              console.log(objectUrl)
              var parentOpener = window.opener;
              window.opener = null;
              window.open(objectUrl, "_blank");
              window.opener = parentOpener;
              }
          });
        }
      };

      
    const defaultItems = PSPDFKit.defaultToolbarItems;
    defaultItems.push(downloadButton)
    console.log(defaultItems);



		PSPDFKit.unload(container) // Ensure that there's only one PSPDFKit instance.
      instance = await PSPDFKit.load({
        // Container where PSPDFKit should be mounted.
        container,
        // The document to open.
        document: document, 

        // Use the public directory URL as a base URL. PSPDFKit will download its library assets from here.
        baseUrl: `${window.location.protocol}//${window.location.host}/public/`
      });

      instance.setToolbarItems(defaultItems.filter((item) => ["signature","custom"].includes(item.type) ));

    })();
    
    return () => PSPDFKit && PSPDFKit.unload(container)
  }, [document]);

  
  
  // This div element will render the document to the DOM.
  return <div ref={containerRef} style={{ display: "block", width: "100vw", height: "100vh" }} />
}